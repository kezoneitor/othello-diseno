(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./app/app-routing.module.ts":
/*!***********************************!*\
  !*** ./app/app-routing.module.ts ***!
  \***********************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _componentes_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./componentes/login-page/login-page.component */ "./app/componentes/login-page/login-page.component.ts");
/* harmony import */ var _componentes_logged_page_logged_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./componentes/logged-page/logged-page.component */ "./app/componentes/logged-page/logged-page.component.ts");
/* harmony import */ var _componentes_register_page_register_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./componentes/register-page/register-page.component */ "./app/componentes/register-page/register-page.component.ts");
/* harmony import */ var _componentes_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./componentes/home-page/home-page.component */ "./app/componentes/home-page/home-page.component.ts");
/* harmony import */ var _componentes_game_mode_game_mode_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./componentes/game-mode/game-mode.component */ "./app/componentes/game-mode/game-mode.component.ts");
/* harmony import */ var _componentes_difficulty_difficulty_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./componentes/difficulty/difficulty.component */ "./app/componentes/difficulty/difficulty.component.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./guards/auth.guard */ "./app/guards/auth.guard.ts");
/* harmony import */ var _componentes_pruebita_pruebita_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./componentes/pruebita/pruebita.component */ "./app/componentes/pruebita/pruebita.component.ts");
/* harmony import */ var _componentes_ingame_ingame_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./componentes/ingame/ingame.component */ "./app/componentes/ingame/ingame.component.ts");
/* harmony import */ var _componentes_session_session_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./componentes/session/session.component */ "./app/componentes/session/session.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    { path: 'login', component: _componentes_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_2__["LoginPageComponent"] },
    { path: 'logged', component: _componentes_logged_page_logged_page_component__WEBPACK_IMPORTED_MODULE_3__["LoggedPageComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'register', component: _componentes_register_page_register_page_component__WEBPACK_IMPORTED_MODULE_4__["RegisterPageComponent"] },
    { path: '', component: _componentes_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_5__["HomePageComponent"] },
    { path: 'game-mode', component: _componentes_game_mode_game_mode_component__WEBPACK_IMPORTED_MODULE_6__["GameModeComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'difficulty', component: _componentes_difficulty_difficulty_component__WEBPACK_IMPORTED_MODULE_7__["DifficultyComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'pruebita', component: _componentes_pruebita_pruebita_component__WEBPACK_IMPORTED_MODULE_9__["PruebitaComponent"] },
    { path: 'ingame', component: _componentes_ingame_ingame_component__WEBPACK_IMPORTED_MODULE_10__["IngameComponent"] },
    { path: 'session', component: _componentes_session_session_component__WEBPACK_IMPORTED_MODULE_11__["SessionComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./app/app.component.html":
/*!********************************!*\
  !*** ./app/app.component.html ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\r\n<div class=\"container\">\r\n    <flash-messages></flash-messages>\r\n</div>\r\n<router-outlet class=\"m-5\"></router-outlet>\r\n"

/***/ }),

/***/ "./app/app.component.scss":
/*!********************************!*\
  !*** ./app/app.component.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./app/app.component.ts":
/*!******************************!*\
  !*** ./app/app.component.ts ***!
  \******************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./app/app.module.ts":
/*!***************************!*\
  !*** ./app/app.module.ts ***!
  \***************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "../node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _componentes_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./componentes/login-page/login-page.component */ "./app/componentes/login-page/login-page.component.ts");
/* harmony import */ var _componentes_logged_page_logged_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./componentes/logged-page/logged-page.component */ "./app/componentes/logged-page/logged-page.component.ts");
/* harmony import */ var _componentes_register_page_register_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./componentes/register-page/register-page.component */ "./app/componentes/register-page/register-page.component.ts");
/* harmony import */ var _componentes_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./componentes/navbar/navbar.component */ "./app/componentes/navbar/navbar.component.ts");
/* harmony import */ var _componentes_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./componentes/home-page/home-page.component */ "./app/componentes/home-page/home-page.component.ts");
/* harmony import */ var _componentes_game_mode_game_mode_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./componentes/game-mode/game-mode.component */ "./app/componentes/game-mode/game-mode.component.ts");
/* harmony import */ var _componentes_difficulty_difficulty_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./componentes/difficulty/difficulty.component */ "./app/componentes/difficulty/difficulty.component.ts");
/* harmony import */ var _componentes_ingame_ingame_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./componentes/ingame/ingame.component */ "./app/componentes/ingame/ingame.component.ts");
/* harmony import */ var _componentes_pruebita_pruebita_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./componentes/pruebita/pruebita.component */ "./app/componentes/pruebita/pruebita.component.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! angular2-flash-messages */ "../node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! angularfire2 */ "../node_modules/angularfire2/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! angularfire2/auth */ "../node_modules/angularfire2/auth/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../environments/environment */ "./environments/environment.ts");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./servicios/auth.service */ "./app/servicios/auth.service.ts");
/* harmony import */ var _servicios_ingame_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./servicios/ingame.service */ "./app/servicios/ingame.service.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./guards/auth.guard */ "./app/guards/auth.guard.ts");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! angularfire2/database */ "../node_modules/angularfire2/database/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _componentes_session_session_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./componentes/session/session.component */ "./app/componentes/session/session.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _componentes_logged_page_logged_page_component__WEBPACK_IMPORTED_MODULE_6__["LoggedPageComponent"],
                _componentes_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
                _componentes_register_page_register_page_component__WEBPACK_IMPORTED_MODULE_7__["RegisterPageComponent"],
                _componentes_login_page_login_page_component__WEBPACK_IMPORTED_MODULE_5__["LoginPageComponent"],
                _componentes_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_9__["HomePageComponent"],
                _componentes_game_mode_game_mode_component__WEBPACK_IMPORTED_MODULE_10__["GameModeComponent"],
                _componentes_difficulty_difficulty_component__WEBPACK_IMPORTED_MODULE_11__["DifficultyComponent"],
                _componentes_pruebita_pruebita_component__WEBPACK_IMPORTED_MODULE_13__["PruebitaComponent"],
                _componentes_ingame_ingame_component__WEBPACK_IMPORTED_MODULE_12__["IngameComponent"],
                _componentes_session_session_component__WEBPACK_IMPORTED_MODULE_23__["SessionComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                angularfire2_auth__WEBPACK_IMPORTED_MODULE_16__["AngularFireAuthModule"],
                angularfire2__WEBPACK_IMPORTED_MODULE_15__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_17__["environment"].firebaseConfig),
                angular2_flash_messages__WEBPACK_IMPORTED_MODULE_14__["FlashMessagesModule"],
                angularfire2_database__WEBPACK_IMPORTED_MODULE_21__["AngularFireDatabaseModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
            ],
            providers: [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_18__["AuthService"], angular2_flash_messages__WEBPACK_IMPORTED_MODULE_14__["FlashMessagesService"], _guards_auth_guard__WEBPACK_IMPORTED_MODULE_20__["AuthGuard"], _servicios_ingame_service__WEBPACK_IMPORTED_MODULE_19__["IngameService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./app/componentes/difficulty/difficulty.component.html":
/*!**************************************************************!*\
  !*** ./app/componentes/difficulty/difficulty.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <body>\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-9 col-md-7 col-lg-5 mx-auto\">\r\n          <div class=\"card card-signin my-5\">\r\n            <div class=\"card-body\">\r\n              <h3 class=\"card-title text-center\">Juega a Othello</h3>\r\n              <h6 class=\"card-title2 text-center\">Dificultad</h6>\r\n              <div (submit)=\"onSubmitLogin()\" class=\"div-signin\">\r\n                \r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Fácil</button>\r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Normal</button>\r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Difícil</button>\r\n                \r\n                <hr class=\"my-4\">\r\n              </div>\r\n              <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" routerLink=\"/game-mode\" type=\"submit\">Atrás</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </body>\r\n"

/***/ }),

/***/ "./app/componentes/difficulty/difficulty.component.scss":
/*!**************************************************************!*\
  !*** ./app/componentes/difficulty/difficulty.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./app/componentes/difficulty/difficulty.component.ts":
/*!************************************************************!*\
  !*** ./app/componentes/difficulty/difficulty.component.ts ***!
  \************************************************************/
/*! exports provided: DifficultyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DifficultyComponent", function() { return DifficultyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DifficultyComponent = /** @class */ (function () {
    function DifficultyComponent() {
    }
    DifficultyComponent.prototype.ngOnInit = function () {
    };
    DifficultyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-difficulty',
            template: __webpack_require__(/*! ./difficulty.component.html */ "./app/componentes/difficulty/difficulty.component.html"),
            styles: [__webpack_require__(/*! ./difficulty.component.scss */ "./app/componentes/difficulty/difficulty.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DifficultyComponent);
    return DifficultyComponent;
}());



/***/ }),

/***/ "./app/componentes/game-mode/game-mode.component.html":
/*!************************************************************!*\
  !*** ./app/componentes/game-mode/game-mode.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <body>\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-9 col-md-7 col-lg-5 mx-auto\">\r\n          <div class=\"card card-signin my-5\">\r\n            <div class=\"card-body\">\r\n              <h3 class=\"card-title text-center\">Juega a Othello</h3>\r\n              <h6 class=\"card-title2 text-center\">Modo de juego</h6>\r\n              <div (submit)=\"onSubmitLogin()\" class=\"div-signin\">\r\n                \r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Reanudar</button>\r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" routerLink=\"/difficulty\" type=\"submit\">J1 vs BOT</button>\r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" routerLink=\"/session\" type=\"submit\">J1 vs J2</button>\r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">BOT vs BOT</button>\r\n                <hr class=\"my-4\">\r\n              </div>\r\n              <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" routerLink=\"/logged\" type=\"submit\">Atrás</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </body>\r\n\r\n"

/***/ }),

/***/ "./app/componentes/game-mode/game-mode.component.scss":
/*!************************************************************!*\
  !*** ./app/componentes/game-mode/game-mode.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./app/componentes/game-mode/game-mode.component.ts":
/*!**********************************************************!*\
  !*** ./app/componentes/game-mode/game-mode.component.ts ***!
  \**********************************************************/
/*! exports provided: GameModeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameModeComponent", function() { return GameModeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GameModeComponent = /** @class */ (function () {
    function GameModeComponent() {
    }
    GameModeComponent.prototype.ngOnInit = function () {
    };
    GameModeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-game-mode',
            template: __webpack_require__(/*! ./game-mode.component.html */ "./app/componentes/game-mode/game-mode.component.html"),
            styles: [__webpack_require__(/*! ./game-mode.component.scss */ "./app/componentes/game-mode/game-mode.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], GameModeComponent);
    return GameModeComponent;
}());



/***/ }),

/***/ "./app/componentes/home-page/home-page.component.html":
/*!************************************************************!*\
  !*** ./app/componentes/home-page/home-page.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html> \r\n<html lang=en>\r\n<head> \r\n\t<meta charset=utf-8>\r\n\t<title> Home Page </title>\r\n\t<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.2.0/css/all.css\" integrity=\"sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ\" crossorigin=\"anonymous\">\r\n</head>\r\n\r\n<body>\r\n\t\t<div class=\"container\">\r\n\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t<div class=\"col-sm-9 col-md-7 col-lg-9 mx-auto\">\r\n\t\t\t\t\t\t\t<div class=\"card card-signin my-5\">\r\n\t\t\t\t\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t\t\t\t\t<section class=\"main\"> \r\n\t\t\t\t\t\t\t\t\t\t<section class=\"articles\">\r\n\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t<article>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<h2>¿Cómo se juega?</h2>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<p>El objetivo del juego es tener más fichas del propio color sobre el tablero al final de la partida.\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tSe emplea un tablero de 8 filas por 8 columnas y 64 fichas idénticas, redondas, blancas por una cara y negras por la otra. Juegan dos jugadores, uno lleva las blancas y el otro las negras. De inicio se colocan cuatro fichas como en el diagrama, dos fichas blancas en D4 y E5, y dos negras en E4 y D5.\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-thumbnail\" src=\"https://sites.google.com/site/asothello/_/rsrc/1255883616155/como-jugar/Reglas1.jpg\" alt=\"sites.google.com\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</article>\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t<article>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<p>Comienzan a mover las negras. Un movimiento consiste en colocar una ficha propia sobre el tablero de forma que 'flanquee' una o varias fichas contrarias. Las fichas flanqueadas son volteadas para mostrar el color propio.\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"img2\" src=\"https://sites.google.com/site/asothello/_/rsrc/1255884205301/como-jugar/Reglas2.jpg\" alt=\"sites.google.com\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</article>\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t<article>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t<p>Es obligatorio voltear todas las fichas flanqueadas entre la ficha que se coloca y las que ya estaban colocadas.\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tUna vez volteadas las fichas el turno pasa al contrario que procede de la misma forma con sus fichas. Si un jugador no tiene ninguna posibilidad de mover, el turno pasa al contrario.\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tLa partida termina cuando ninguno de los dos jugadores puede mover. Normalmente cuando el tablero está lleno o prácticamente lleno.\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tGana el jugador que acaba con más fichas propias sobre el tablero. Es posible el empate.</p>\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t</article>\r\n\r\n\t\t\t\t\t\t\t\t\t\t</section>\r\n\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t</section>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<footer>\r\n\t\t\t\t\t\t<i id=\"ang\" class=\"fab fa-angular\">&nbsp; &nbsp;Vinicio - Keslerth - Fabian</i>\r\n\r\n\t\t\t\t\t</footer>\r\n\r\n</body>\r\n</html>"

/***/ }),

/***/ "./app/componentes/home-page/home-page.component.scss":
/*!************************************************************!*\
  !*** ./app/componentes/home-page/home-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  background: #9CECFB;\n  /* fallback for old browsers */\n  /* Chrome 10-25, Safari 5.1-6 */\n  background: linear-gradient(to right, #0052D4, #65C7F7, #9CECFB);\n  /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */ }\n\n.img2 {\n  width: 70%;\n  height: 70%; }\n\n.img2 {\n  width: 100%;\n  height: 100%; }\n\nfooter {\n  background: #000;\n  color: #fff;\n  clear: both;\n  padding: 10px 0px;\n  text-align: center;\n  /*Pone el texto en el centro*/ }\n\n.fab {\n  color: #fff;\n  text-shadow: 1px 1px 1px #ccc;\n  font-size: 1.5em; }\n"

/***/ }),

/***/ "./app/componentes/home-page/home-page.component.ts":
/*!**********************************************************!*\
  !*** ./app/componentes/home-page/home-page.component.ts ***!
  \**********************************************************/
/*! exports provided: HomePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageComponent", function() { return HomePageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomePageComponent = /** @class */ (function () {
    function HomePageComponent() {
    }
    HomePageComponent.prototype.ngOnInit = function () {
    };
    HomePageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-page',
            template: __webpack_require__(/*! ./home-page.component.html */ "./app/componentes/home-page/home-page.component.html"),
            styles: [__webpack_require__(/*! ./home-page.component.scss */ "./app/componentes/home-page/home-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HomePageComponent);
    return HomePageComponent;
}());



/***/ }),

/***/ "./app/componentes/ingame/ingame.component.html":
/*!******************************************************!*\
  !*** ./app/componentes/ingame/ingame.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row w-100 m-0\">\r\n  <div class=\"col-sm-10 col-md-7 col-lg-7 mx-auto  mt-5 d-flex justify-content-center\">\r\n    <table class=\"tablero\">\r\n        <tr *ngFor=\"let fila of this.juego.tablero; let i = index\">\r\n                <td *ngFor=\"let casilla of fila; let j = index\">\r\n                  <div [style.background]=\"(i+j)%2 == 0 ? j1_color : j2_color\" class=\"d-flex justify-content-center\">\r\n                    \r\n                    <button *ngIf=\"casilla == 0\" (click)=\"realizarJugada(i, j)\" [style.background]=\"this.juego.jugActual == 1 ? j1_color : j2_color\" class=\"color{{ casilla }}{{ this.juego.jugActual }} w-100\"></button>\r\n  \r\n                    <button *ngIf=\"casilla != 0\" [style.background]=\"casilla == 1 ? j1_color : j2_color\" class=\"color{{ casilla }} w-100\"></button>\r\n                  \r\n                  </div>\r\n                </td>\r\n        </tr>\r\n    </table>\r\n  </div>\r\n  <div class=\"col-sm-10 col-md-4 col-lg-4 mx-auto d-flex justify-content-center\">\r\n    <div class=\"card card-signin my-5\">\r\n        <div class=\"p-2 text-center\">\r\n            <i class=\"far fa-flushed emoji\"></i>\r\n            <h2>Juego</h2>\r\n        </div>\r\n        <div class=\"pl-4 pr-4\">\r\n                <div class=\"row p-2\">\r\n                    <div class=\"col\">\r\n                        <h5>Fichas J1:</h5>\r\n                    </div>\r\n                    <div class=\"col\">\r\n                        #CantFichasJ1\r\n                    </div>\r\n                </div>\r\n                <div class=\"row p-2\">\r\n                    <div class=\"col\">\r\n                        <h5>Fichas J2:</h5>\r\n                    </div>\r\n                    <div class=\"col\">\r\n                        #CantFichasJ2\r\n                    </div>\r\n                </div>  \r\n                <div class=\"text-center\">\r\n                    <h4>Jugador actual:</h4>\r\n                </div>\r\n                <div class=\"text-center\">\r\n                        #NombreJugador\r\n                </div>\r\n            </div>\r\n\r\n        <hr class=\"ml-5 mr-5\">\r\n      <!-- Titulo estadisticas -->\r\n      <div class=\"p-2 text-center\">\r\n            <h2>Estadísticas</h2>\r\n      </div>\r\n      <!-- nombre del jugador 1-->\r\n        <div class=\"p-2\">\r\n            <h4> #nombre del jugador1# </h4>\r\n        </div>\r\n      <!-- contiene las estadisticas jugador 1-->\r\n      <div class=\"pl-4 pr-4\">\r\n            <div class=\"row p-2\">\r\n                <div class=\"col\">\r\n                    <h5>Partidas ganadas J1:</h5>\r\n                </div>\r\n                <div class=\"col\">\r\n                    #Ganesj1\r\n                </div>\r\n            </div>\r\n            <div class=\"row p-2\">\r\n                <div class=\"col\">\r\n                    <h5>Partidas perdidas J1:</h5>\r\n                </div>\r\n                <div class=\"col\">\r\n                    #Perdidasj1\r\n                </div>\r\n            </div>\r\n            <div class=\"row p-2\">\r\n                <div class=\"col\">\r\n                    <h5>Partidas Empatadas :</h5>\r\n                </div>\r\n                <div class=\"col\">\r\n                #empates\r\n                </div>\r\n            </div>    \r\n        </div>\r\n      <!-- nombre del jugador 2-->\r\n      <div class=\"p-2\">\r\n            <h4> #nombre del jugador2# </h4>\r\n        </div>\r\n        <div class=\"pl-4 pr-4\">\r\n            <div class=\"row p-2\">\r\n                <div class=\"col\">\r\n                    <h5>Partidas ganadas J2:</h5>\r\n                </div>\r\n                <div class=\"col\">\r\n                    #Ganesj2\r\n                </div>\r\n            </div>\r\n            <div class=\"row p-2\">\r\n                <div class=\"col\">\r\n                    <h5>Partidas perdidas J2:</h5>\r\n                </div>\r\n                <div class=\"col\">\r\n                    #Perdidasj2\r\n                </div>\r\n            </div>\r\n            <div class=\"row p-2\">\r\n                <div class=\"col\">\r\n                    <h5>Partidas Empatadas:</h5>\r\n                </div>\r\n                <div class=\"col\">\r\n                    #empates\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./app/componentes/ingame/ingame.component.scss":
/*!******************************************************!*\
  !*** ./app/componentes/ingame/ingame.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tablero {\n  width: 550px;\n  height: 550px;\n  margin-top: 3%;\n  text-align: center; }\n\n.tablero td {\n  width: 50px;\n  height: 50px;\n  text-align: center;\n  padding: 0;\n  margin: 0;\n  border: solid black; }\n\n.tablero div {\n  width: 100%;\n  height: 100%; }\n\n.tablero button {\n  margin: 10%; }\n\nbutton.color1 {\n  border: outset 3px;\n  border-radius: 50%;\n  border-color: #7d8c8e; }\n\nbutton.color01 {\n  opacity: 0; }\n\nbutton.color01:hover {\n  opacity: 1;\n  border: outset 3px;\n  border-radius: 50%;\n  border-color: #7d8c8e; }\n\nbutton.color01:active {\n  opacity: 1;\n  border: outset 3px;\n  border-radius: 50%;\n  border-color: #004edc; }\n\nbutton.color2 {\n  border: outset 3px;\n  border-radius: 50%;\n  border-color: #7d8c8e; }\n\nbutton.color02 {\n  opacity: 0; }\n\nbutton.color02:hover {\n  opacity: 1;\n  border: outset 3px;\n  border-radius: 50%;\n  border-color: #7d8c8e; }\n\nbutton.color02:active {\n  opacity: 1;\n  border: outset 3px;\n  border-radius: 50%;\n  border-color: #004edc; }\n\n.emoji {\n  font-size: 40px;\n  background: linear-gradient(to right, blue, orange);\n  border-radius: 45px; }\n"

/***/ }),

/***/ "./app/componentes/ingame/ingame.component.ts":
/*!****************************************************!*\
  !*** ./app/componentes/ingame/ingame.component.ts ***!
  \****************************************************/
/*! exports provided: IngameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngameComponent", function() { return IngameComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_juego__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/juego */ "./app/models/juego.ts");
/* harmony import */ var _servicios_ingame_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../servicios/ingame.service */ "./app/servicios/ingame.service.ts");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../servicios/auth.service */ "./app/servicios/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IngameComponent = /** @class */ (function () {
    function IngameComponent(authService, _ingameService) {
        this.authService = authService;
        this._ingameService = _ingameService;
        this.juego = new _models_juego__WEBPACK_IMPORTED_MODULE_1__["Juego"](1, 1, 2, this.authService.size, [], 1);
        console.log(this.authService.jug2);
        this.j1_color = this.authService.jug1;
        this.j2_color = this.authService.jug2;
    }
    IngameComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._ingameService.tipoJuego(this.juego).subscribe(function (result) {
            _this.juego.tablero = result.tablero;
            console.log(result);
        }, function (error) {
            console.log(error);
        });
    };
    IngameComponent.prototype.realizarJugada = function (x, y) {
        var _this = this;
        this._ingameService.realizarJugada(x, y, this.juego.jugActual).subscribe(function (result) {
            _this.juego.tablero = result.tablero;
            if (result.jugadaValida) {
                _this.juego.jugActual = result.jugActual;
            }
            console.log(result);
        }, function (error) {
            console.log(error);
        });
    };
    IngameComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ingame',
            template: __webpack_require__(/*! ./ingame.component.html */ "./app/componentes/ingame/ingame.component.html"),
            styles: [__webpack_require__(/*! ./ingame.component.scss */ "./app/componentes/ingame/ingame.component.scss")],
            providers: [_servicios_ingame_service__WEBPACK_IMPORTED_MODULE_2__["IngameService"]]
        }),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _servicios_ingame_service__WEBPACK_IMPORTED_MODULE_2__["IngameService"]])
    ], IngameComponent);
    return IngameComponent;
}());



/***/ }),

/***/ "./app/componentes/logged-page/logged-page.component.html":
/*!****************************************************************!*\
  !*** ./app/componentes/logged-page/logged-page.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <body>\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-9 col-md-7 col-lg-5 mx-auto\">\r\n          <div class=\"card card-signin my-5\">\r\n            <div class=\"card-body\">\r\n                \r\n              <h5 class=\"card-title text-center\">Juega a Othello!</h5>\r\n              \r\n              <div class=\"div-play\">\r\n                <i class=\"far fa-play-circle d-flex justify-content-center\"></i>\r\n              </div>\r\n              \r\n              <div class=\"div-signin\">\r\n              <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" routerLink=\"/game-mode\" type=\"submit\">Jugar Othello!</button>\r\n              <hr class=\"my-4\">\r\n              <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" routerLink=\"\" type=\"submit\">Mi perfil</button>              \r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </body>\r\n\r\n"

/***/ }),

/***/ "./app/componentes/logged-page/logged-page.component.scss":
/*!****************************************************************!*\
  !*** ./app/componentes/logged-page/logged-page.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-signin .card-title {\n  margin-bottom: 5rem;\n  font-weight: 300;\n  font-size: 1.5rem; }\n\n.div-signin {\n  margin-top: 5rem;\n  font-weight: 300;\n  font-size: 1.5rem; }\n\n.far {\n  color: #228B22;\n  font-size: 110px;\n  display: block; }\n"

/***/ }),

/***/ "./app/componentes/logged-page/logged-page.component.ts":
/*!**************************************************************!*\
  !*** ./app/componentes/logged-page/logged-page.component.ts ***!
  \**************************************************************/
/*! exports provided: LoggedPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoggedPageComponent", function() { return LoggedPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./app/servicios/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoggedPageComponent = /** @class */ (function () {
    function LoggedPageComponent(authService) {
        this.authService = authService;
    }
    LoggedPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getAuth().subscribe(function (auth) {
            _this.nombreUsuario = auth.displayName;
            _this.email = auth.email;
            console.log(auth.displayName);
        });
    };
    LoggedPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-logged-page',
            template: __webpack_require__(/*! ./logged-page.component.html */ "./app/componentes/logged-page/logged-page.component.html"),
            styles: [__webpack_require__(/*! ./logged-page.component.scss */ "./app/componentes/logged-page/logged-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], LoggedPageComponent);
    return LoggedPageComponent;
}());



/***/ }),

/***/ "./app/componentes/login-page/login-page.component.html":
/*!**************************************************************!*\
  !*** ./app/componentes/login-page/login-page.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <body>\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-9 col-md-7 col-lg-5 mx-auto\">\r\n          <div class=\"card card-signin my-5\">\r\n            <div class=\"card-body\">\r\n              <h5 class=\"card-title text-center\">Othello</h5>\r\n              <form (submit)=\"onSubmitLogin()\" class=\"form-signin\">\r\n                <div class=\"form-label-group\">\r\n                  <input type=\"email\" id=\"inputEmail\" [(ngModel)]=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Email address\" required autofocus>\r\n                  <label for=\"inputEmail\">Email address</label>\r\n                </div>\r\n  \r\n                <div class=\"form-label-group\">\r\n                  <input type=\"password\" id=\"inputPassword\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" required>\r\n                  <label for=\"inputPassword\">Password</label>\r\n                </div>\r\n  \r\n                <div class=\"custom-control custom-checkbox mb-3\">\r\n                  <input type=\"checkbox\" class=\"custom-control-input\" id=\"customCheck1\">\r\n                  <label class=\"custom-control-label\" for=\"customCheck1\">Remember password</label>\r\n                </div>\r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Ingresar</button>\r\n                <hr class=\"my-4\">\r\n              </form>\r\n                <button (click)=\"onClickGoogleLogin()\" class=\"btn btn-lg btn-google btn-block text-uppercase\" type=\"submit\"><i class=\"fab fa-google mr-2\"></i>Ingresar con Google</button>\r\n                <button (click)=\"onClickFacebookLogin()\" class=\"btn btn-lg btn-facebook btn-block text-uppercase\" type=\"submit\"> <i class=\"fab fa-facebook-f mr-2\"></i> Ingresar con Facebook</button>\r\n              \r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </body>\r\n\r\n\r\n"

/***/ }),

/***/ "./app/componentes/login-page/login-page.component.scss":
/*!**************************************************************!*\
  !*** ./app/componentes/login-page/login-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-google {\n  color: white;\n  background-color: #ea4335; }\n\n.btn-facebook {\n  color: white;\n  background-color: #3b5998; }\n\n.btn-google, .btn-facebook, .btn-primary {\n  font-size: 80%;\n  border-radius: 5rem;\n  letter-spacing: .1rem;\n  font-weight: bold;\n  padding: 1rem;\n  transition: all 0.2s; }\n"

/***/ }),

/***/ "./app/componentes/login-page/login-page.component.ts":
/*!************************************************************!*\
  !*** ./app/componentes/login-page/login-page.component.ts ***!
  \************************************************************/
/*! exports provided: LoginPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageComponent", function() { return LoginPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./app/servicios/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-flash-messages */ "../node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPageComponent = /** @class */ (function () {
    function LoginPageComponent(router, authService, flashMensaje) {
        this.router = router;
        this.authService = authService;
        this.flashMensaje = flashMensaje;
    }
    LoginPageComponent.prototype.ngOnInit = function () {
    };
    LoginPageComponent.prototype.onClickFacebookLogin = function () {
        var _this = this;
        this.authService.loginFacebook()
            .then(function (res) {
            _this.router.navigate(['/logged']);
        }).catch(function (err) { return console.log(err.message); });
    };
    LoginPageComponent.prototype.onClickGoogleLogin = function () {
        var _this = this;
        this.authService.loginGoogle()
            .then(function (res) {
            _this.router.navigate(['/logged']);
        }).catch(function (err) { return console.log(err.message); });
    };
    LoginPageComponent.prototype.onSubmitLogin = function () {
        var _this = this;
        this.authService.loginEmail(this.email, this.password)
            .then(function (res) {
            /* this.flashMensaje.show('Ingreso éxitoso.', {cssClass: 'alert-success',
             timeout:4000});*/
            _this.router.navigate(['/logged']);
        }).catch(function (err) {
            /* this.flashMensaje.show(err.message, {cssClass: 'alert-danger',
             timeout:4000});*/
            _this.router.navigate(['/login']);
        });
    };
    LoginPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-page',
            template: __webpack_require__(/*! ./login-page.component.html */ "./app/componentes/login-page/login-page.component.html"),
            styles: [__webpack_require__(/*! ./login-page.component.scss */ "./app/componentes/login-page/login-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__["FlashMessagesService"]])
    ], LoginPageComponent);
    return LoginPageComponent;
}());



/***/ }),

/***/ "./app/componentes/navbar/navbar.component.html":
/*!******************************************************!*\
  !*** ./app/componentes/navbar/navbar.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<body>\r\n  <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark fixed-top\">\r\n    <a class=\"navbar-brand\" routerLink=\"/\"><span class=\"text-primary\">OTHELLO</span></a>\r\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor02\" aria-controls=\"navbarColor02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n  \r\n    <div class=\"collapse navbar-collapse\" id=\"navbarColor02\">\r\n      <ul class=\"navbar-nav mr-auto\">\r\n        <li class=\"nav-item\" *ngIf=\"!isLogin || router.url === '/login' || router.url === '/register' || router.url === '/'\">\r\n          <a class=\"nav-link\" routerLink=\"/login\">Ingresar</a>\r\n        </li>\r\n        <li class=\"nav-item\" *ngIf=\"!isLogin || router.url === '/login' || router.url === '/register' || router.url === '/'\">\r\n          <a class=\"nav-link\" routerLink=\"/register\">Registrarse</a>\r\n        </li>\r\n        <li class=\"nav-item\" *ngIf=\"isLogin && router.url != '/login' && router.url != '/register' && router.url != '/'\" >\r\n          <a class=\"nav-link\" routerLink=\"/logged\">Inicio</a>\r\n        </li>\r\n        <li class=\"nav-item\" *ngIf=\"isLogin && router.url != '/login' && router.url != '/register' && router.url != '/'\" >\r\n          <a class=\"nav-link\" routerLink=\"\" (click)=\"onClickLogout()\" >Salir</a>\r\n        </li>\r\n      </ul>\r\n      <ul class=\"navbar-nav mx-auto\" *ngIf=\"isLogin && router.url != '/login' && router.url != '/register' && router.url != '/'\">\r\n        <li class=\"nav-item\" *ngIf=\"nombreUsuario!=null\">\r\n          <a class=\"nav-link\">Usuario: {{nombreUsuario}}</a>\r\n        </li>\r\n        <li class=\"nav-item\" *ngIf=\"nombreUsuario==null && router.url != '/login' && router.url != '/register' && router.url != '/'\">\r\n          <a class=\"nav-link\">Usuario: {{emailUsuario}}</a>\r\n        </li>\r\n      </ul>\r\n  \r\n    </div>\r\n  </nav>\r\n</body>\r\n\r\n"

/***/ }),

/***/ "./app/componentes/navbar/navbar.component.scss":
/*!******************************************************!*\
  !*** ./app/componentes/navbar/navbar.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./app/componentes/navbar/navbar.component.ts":
/*!****************************************************!*\
  !*** ./app/componentes/navbar/navbar.component.ts ***!
  \****************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./app/servicios/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    //metodo cuando la app carga
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getAuth().subscribe(function (auth) {
            if (auth) {
                _this.isLogin = true;
                console.log('login: ' + _this.isLogin);
                _this.nombreUsuario = auth.displayName;
                _this.emailUsuario = auth.email;
            }
            else {
                _this.isLogin = false;
                console.log('login: ' + _this.isLogin);
            }
        });
    };
    NavbarComponent.prototype.onClickLogout = function () {
        console.log('AQUI TOY ');
        this.authService.logout();
        this.router.navigate(['/']);
        console.log('login: ' + this.isLogin);
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./app/componentes/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./app/componentes/navbar/navbar.component.scss")]
        }),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./app/componentes/pruebita/pruebita.component.html":
/*!**********************************************************!*\
  !*** ./app/componentes/pruebita/pruebita.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n    <form class='form-signin' (submit)=\"onSubmitGame()\">\r\n\r\n  <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">guarda partida</button>\r\n    </form>>\r\n\r\n"

/***/ }),

/***/ "./app/componentes/pruebita/pruebita.component.scss":
/*!**********************************************************!*\
  !*** ./app/componentes/pruebita/pruebita.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./app/componentes/pruebita/pruebita.component.ts":
/*!********************************************************!*\
  !*** ./app/componentes/pruebita/pruebita.component.ts ***!
  \********************************************************/
/*! exports provided: PruebitaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PruebitaComponent", function() { return PruebitaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_game__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/game */ "./app/models/game.ts");
/* harmony import */ var _servicios_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../servicios/game.service */ "./app/servicios/game.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PruebitaComponent = /** @class */ (function () {
    function PruebitaComponent(gameService) {
        this.gameService = gameService;
        this.game = new _models_game__WEBPACK_IMPORTED_MODULE_1__["Game"]();
    }
    PruebitaComponent.prototype.ngOnInit = function () {
        this.gameService.getProducts();
    };
    PruebitaComponent.prototype.onSubmitGame = function () {
        this.game.$code = 123;
        this.game.player1 = 'Juan';
        this.game.player2 = 'Carlos';
        this.game.pts1 = 12;
        this.game.pts2 = 11;
        console.log(this.game);
        this.gameService.insertGame(this.game);
    };
    PruebitaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pruebita',
            template: __webpack_require__(/*! ./pruebita.component.html */ "./app/componentes/pruebita/pruebita.component.html"),
            styles: [__webpack_require__(/*! ./pruebita.component.scss */ "./app/componentes/pruebita/pruebita.component.scss")]
        }),
        __metadata("design:paramtypes", [_servicios_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"]])
    ], PruebitaComponent);
    return PruebitaComponent;
}());



/***/ }),

/***/ "./app/componentes/register-page/register-page.component.html":
/*!********************************************************************!*\
  !*** ./app/componentes/register-page/register-page.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <body>\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-9 col-md-7 col-lg-5 mx-auto\">\r\n          <div class=\"card card-signin my-5\">\r\n            <div class=\"card-body\">\r\n              <h5 class=\"card-title text-center\">Othello</h5>\r\n              <form (submit)=\"onSubmitAddUser()\" class=\"form-signin\">\r\n                  <div class=\"form-label-group\">\r\n                      <input type=\"text\" id=\"username\" class=\"form-control\" placeholder=\"username\" required autofocus>\r\n                      <label for=\"username\">Username</label>\r\n                  </div>\r\n                <div class=\"form-label-group\">\r\n                  <input type=\"email\" id=\"inputEmail\" [(ngModel)]=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Email address\" required autofocus>\r\n                  <label for=\"inputEmail\">Email address</label>\r\n                </div>\r\n  \r\n                <div class=\"form-label-group\">\r\n                  <input type=\"password\" id=\"inputPassword\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" required>\r\n                  <label for=\"inputPassword\">Password</label>\r\n                </div>\r\n\r\n                <div class=\"form-label-group\">\r\n                  <input type=\"password\" id=\"confirmPassword\" [(ngModel)]=\"confirmPassword\" name=\"confirmPassword\" class=\"form-control\" placeholder=\"confirm\" required>\r\n                  <label for=\"confirmPassword\">Confirm Password</label>\r\n                </div>\r\n                <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Sign Up</button>\r\n                \r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </body>\r\n\r\n"

/***/ }),

/***/ "./app/componentes/register-page/register-page.component.scss":
/*!********************************************************************!*\
  !*** ./app/componentes/register-page/register-page.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./app/componentes/register-page/register-page.component.ts":
/*!******************************************************************!*\
  !*** ./app/componentes/register-page/register-page.component.ts ***!
  \******************************************************************/
/*! exports provided: RegisterPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageComponent", function() { return RegisterPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./app/servicios/auth.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-flash-messages */ "../node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { CONNREFUSED } from 'dns';

var RegisterPageComponent = /** @class */ (function () {
    function RegisterPageComponent(authService, router, flashMensaje) {
        this.authService = authService;
        this.router = router;
        this.flashMensaje = flashMensaje;
    }
    RegisterPageComponent.prototype.ngOnInit = function () {
    };
    RegisterPageComponent.prototype.onSubmitAddUser = function () {
        var _this = this;
        if (this.password != this.confirmPassword) {
            console.log(this.confirmPassword);
            console.log(this.password);
            alert("Contraseñas deben coincidir!");
        }
        else {
            this.authService.registerUser(this.email, this.password)
                .then(function (res) {
                /*this.flashMensaje.show('Usuario creado correctamente.', {cssClass: 'alert-success',
                timeout:4000});*/
                _this.router.navigate(['/logged']);
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    RegisterPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register-page',
            template: __webpack_require__(/*! ./register-page.component.html */ "./app/componentes/register-page/register-page.component.html"),
            styles: [__webpack_require__(/*! ./register-page.component.scss */ "./app/componentes/register-page/register-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__["FlashMessagesService"]])
    ], RegisterPageComponent);
    return RegisterPageComponent;
}());



/***/ }),

/***/ "./app/componentes/session/session.component.html":
/*!********************************************************!*\
  !*** ./app/componentes/session/session.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "    <div class=\"row w-100\">\r\n      <div class=\"col-sm-9 col-md-7 col-lg-6 mx-auto d-flex justify-content-center\">\r\n              <div class=\"card card-signin my-5\">\r\n                <div class=\"card-body\">\r\n                  <h5 class=\"card-title text-center\">Crear Sala</h5>\r\n                  <form  (submit)=\"sessionData()\" class=\"form-signin\" id=\"datos\" name=\"datos\">\r\n                      <div class=\"form-label-group\">\r\n                          <input  type=\"text\" id=\"size\" name=\"size\" class=\"form-control\" placeholder=\"size\" required [(ngModel)]=\"size\" >\r\n                   \r\n                          <label for=\"size\">Tamaño</label>\r\n                      </div>\r\n                      <div class=\"form-row align-items-center\">\r\n                        <div class=\"col-auto my-1\">\r\n                          <label class=\"mr-sm-2\" for=\"jug1\">Color de jugador 1</label>\r\n                          <select [(ngModel)]=\"jug1\" class=\"custom-select mr-sm-2\" name=\"jug1\" id=\"jug1\">\r\n                            <option value=\"#000000\" style=\"background-color: Black;color: #FFFFFF;\">Black</option>\r\n                            <option value=\"#808080\" style=\"background-color: Gray;\">Gray</option>\r\n                            <option value=\"#A9A9A9\" style=\"background-color: DarkGray;\">DarkGray</option>\r\n                            <option value=\"#D3D3D3\" style=\"background-color: LightGrey;\">LightGray</option>\r\n                            <option value=\"#FFFFFF\" style=\"background-color: White;\">White</option>\r\n                            <option value=\"#7FFFD4\" style=\"background-color: Aquamarine;\">Aquamarine</option>\r\n                            <option value=\"#0000FF\" style=\"background-color: Blue;\">Blue</option>\r\n                            <option value=\"#000080\" style=\"background-color: Navy;color: #FFFFFF;\">Navy</option>\r\n                            <option value=\"#800080\" style=\"background-color: Purple;color: #FFFFFF;\">Purple</option>\r\n                            <option value=\"#FF1493\" style=\"background-color: DeepPink;\">DeepPink</option>\r\n                            <option value=\"#EE82EE\" style=\"background-color: Violet;\">Violet</option>\r\n                            <option value=\"#FFC0CB\" style=\"background-color: Pink;\">Pink</option>\r\n                            <option value=\"#006400\" style=\"background-color: DarkGreen;color: #FFFFFF;\">DarkGreen</option>\r\n                            <option value=\"#008000\" style=\"background-color: Green;color: #FFFFFF;\">Green</option>\r\n                            <option value=\"#9ACD32\" style=\"background-color: YellowGreen;\">YellowGreen</option>\r\n                            <option value=\"#FFFF00\" style=\"background-color: Yellow;\">Yellow</option>\r\n                            <option value=\"#FFA500\" style=\"background-color: Orange;\">Orange</option>\r\n                            <option value=\"#FF0000\" style=\"background-color: Red;\">Red</option>\r\n                            <option value=\"#A52A2A\" style=\"background-color: Brown;\">Brown</option>\r\n                            <option value=\"#DEB887\" style=\"background-color: BurlyWood;\">BurlyWood</option>\r\n                            <option value=\"#F5F5DC\" style=\"background-color: Beige;\">Beige</option>\r\n                          </select>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"form-row align-items-center\">\r\n                        <div class=\"col-auto my-1\">\r\n                          <label class=\"mr-sm-2\" for=\"jug2\">Color de jugador 2</label>\r\n                          <select [(ngModel)]=\"jug2\" class=\"custom-select mr-sm-2\" name=\"jug2\" id=\"jug2\">\r\n                            <option value=\"#000000\" style=\"background-color: Black;color: #FFFFFF;\">Black</option>\r\n                          <option value=\"#808080\" style=\"background-color: Gray;\">Gray</option>\r\n                          <option value=\"#A9A9A9\" style=\"background-color: DarkGray;\">DarkGray</option>\r\n                          <option value=\"#D3D3D3\" style=\"background-color: LightGrey;\">LightGray</option>\r\n                          <option value=\"#FFFFFF\" style=\"background-color: White;\">White</option>\r\n                          <option value=\"#7FFFD4\" style=\"background-color: Aquamarine;\">Aquamarine</option>\r\n                          <option value=\"#0000FF\" style=\"background-color: Blue;\">Blue</option>\r\n                          <option value=\"#000080\" style=\"background-color: Navy;color: #FFFFFF;\">Navy</option>\r\n                          <option value=\"#800080\" style=\"background-color: Purple;color: #FFFFFF;\">Purple</option>\r\n                          <option value=\"#FF1493\" style=\"background-color: DeepPink;\">DeepPink</option>\r\n                          <option value=\"#EE82EE\" style=\"background-color: Violet;\">Violet</option>\r\n                          <option value=\"#FFC0CB\" style=\"background-color: Pink;\">Pink</option>\r\n                          <option value=\"#006400\" style=\"background-color: DarkGreen;color: #FFFFFF;\">DarkGreen</option>\r\n                          <option value=\"#008000\" style=\"background-color: Green;color: #FFFFFF;\">Green</option>\r\n                          <option value=\"#9ACD32\" style=\"background-color: YellowGreen;\">YellowGreen</option>\r\n                          <option value=\"#FFFF00\" style=\"background-color: Yellow;\">Yellow</option>\r\n                          <option value=\"#FFA500\" style=\"background-color: Orange;\">Orange</option>\r\n                          <option value=\"#FF0000\" style=\"background-color: Red;\">Red</option>\r\n                          <option value=\"#A52A2A\" style=\"background-color: Brown;\">Brown</option>\r\n                          <option value=\"#DEB887\" style=\"background-color: BurlyWood;\">BurlyWood</option>\r\n                          <option value=\"#F5F5DC\" style=\"background-color: Beige;\">Beige</option>\r\n                          </select>\r\n                        </div>\r\n                      </div>\r\n                    <label class=\"ml-5 mr-5\" for=\"datos\">Codigo de mesa: HJ43</label>\r\n                  \r\n                    <button  class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Crear</button>\r\n                    \r\n                  </form>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          \r\n        \r\n              <div class=\"col-sm-9 col-md-7 col-lg-6 mx-auto d-flex justify-content-center\">\r\n                <div class=\"card card-signin my-5\">\r\n                  <div class=\"card-body\">\r\n                    <h5 class=\"card-title text-center\">Unirse a Sala</h5>\r\n                      <input type=\"text\" id=\"myInput\" onkeyup=\"myFunction()\" placeholder=\"Search for names..\" title=\"Type in a name\">\r\n                      <div class=\"scroll\">\r\n                      <table id=\"myTable\">\r\n                        <tr class=\"header\">\r\n                          <th style=\"width:60%;\">Usuario</th>\r\n                          <th style=\"width:40%;\">Estado</th>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>Alfreds Futterkiste</td>\r\n                          <td>Ocupado</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>Berglunds snabbkop</td>\r\n                          <td>Ocupado</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>Island Trading</td>\r\n                          <td>Ocupado</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>Koniglich Essen</td>\r\n                          <td>Ocupado</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>Laughing Bacchus Winecellars</td>\r\n                          <td>Ocupado</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>Magazzini Alimentari Riuniti</td>\r\n                          <td>Ocupado</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>North/South</td>\r\n                          <td>Ocupado</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>Paris specialites</td>\r\n                          <td>Ocupado</td>\r\n                        </tr>\r\n                        \r\n                      </table>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n        </div>\r\n      "

/***/ }),

/***/ "./app/componentes/session/session.component.scss":
/*!********************************************************!*\
  !*** ./app/componentes/session/session.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-secondary {\n  border-color: #CED4DA; }\n\n.ml-1 {\n  margin-left: 40%; }\n\n.scroll {\n  height: 200px !important;\n  overflow: scroll; }\n"

/***/ }),

/***/ "./app/componentes/session/session.component.ts":
/*!******************************************************!*\
  !*** ./app/componentes/session/session.component.ts ***!
  \******************************************************/
/*! exports provided: SessionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SessionComponent", function() { return SessionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../servicios/auth.service */ "./app/servicios/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _node_modules_angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/@angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SessionComponent = /** @class */ (function () {
    function SessionComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    SessionComponent.prototype.ngOnInit = function () {
        this.size = undefined;
        this.form = new _node_modules_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            size: new _node_modules_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](undefined, [
                _node_modules_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
            ]),
        });
    };
    SessionComponent.prototype.sessionData = function () {
        this.authService.jug1 = this.jug1;
        this.authService.jug2 = this.jug2;
        this.authService.size = this.size;
        this.router.navigate(['/ingame']);
    };
    SessionComponent.prototype.myFunction = function () {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                }
                else {
                    tr[i].style.display = "none";
                }
            }
        }
    };
    SessionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-session',
            template: __webpack_require__(/*! ./session.component.html */ "./app/componentes/session/session.component.html"),
            styles: [__webpack_require__(/*! ./session.component.scss */ "./app/componentes/session/session.component.scss")]
        }),
        __metadata("design:paramtypes", [_servicios_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], SessionComponent);
    return SessionComponent;
}());



/***/ }),

/***/ "./app/guards/auth.guard.ts":
/*!**********************************!*\
  !*** ./app/guards/auth.guard.ts ***!
  \**********************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_add_operator_do__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/do */ "../node_modules/rxjs-compat/_esm5/add/operator/do.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "../node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_take__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/take */ "../node_modules/rxjs-compat/_esm5/add/operator/take.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularfire2/auth */ "../node_modules/angularfire2/auth/index.js");
/* harmony import */ var _servicios_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../servicios/auth.service */ "./app/servicios/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, afAuth, authService) {
        this.router = router;
        this.afAuth = afAuth;
        this.authService = authService;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        var _this = this;
        return this.authService.afAuth.authState
            .take(1)
            .map(function (authState) { return !!authState; })
            .do(function (autheticated) {
            if (!autheticated) {
                _this.router.navigate(['/login']);
            }
        });
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            angularfire2_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"],
            _servicios_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./app/models/game.ts":
/*!****************************!*\
  !*** ./app/models/game.ts ***!
  \****************************/
/*! exports provided: Game */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Game", function() { return Game; });
var Game = /** @class */ (function () {
    function Game() {
    }
    return Game;
}());



/***/ }),

/***/ "./app/models/juego.ts":
/*!*****************************!*\
  !*** ./app/models/juego.ts ***!
  \*****************************/
/*! exports provided: Juego */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Juego", function() { return Juego; });
var Juego = /** @class */ (function () {
    function Juego(tipo, jugador1, jugador2, dimension, tablero, jugActual) {
        this.tipo = tipo;
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        this.dimension = dimension;
        this.tablero = tablero;
        this.jugActual = jugActual;
    }
    return Juego;
}());



/***/ }),

/***/ "./app/servicios/auth.service.ts":
/*!***************************************!*\
  !*** ./app/servicios/auth.service.ts ***!
  \***************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/app */ "../node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/auth */ "../node_modules/angularfire2/auth/index.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "../node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { resolve } from '../../../node_modules/@types/q';

var AuthService = /** @class */ (function () {
    function AuthService(afAuth) {
        this.afAuth = afAuth;
    }
    AuthService.prototype.loginGoogle = function () {
        return this.afAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_1__["auth"].GoogleAuthProvider());
    };
    AuthService.prototype.loginFacebook = function () {
        return this.afAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_1__["auth"].FacebookAuthProvider());
    };
    AuthService.prototype.registerUser = function (email, pass) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.createUserWithEmailAndPassword(email, pass)
                .then(function (userData) { return resolve(userData); }, function (err) { return reject(err); });
        });
    };
    AuthService.prototype.loginEmail = function (email, pass) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signInWithEmailAndPassword(email, pass)
                .then(function (userData) { return resolve(userData); }, function (err) { return reject(err); });
        });
    };
    //comprueba cuando hay alguien loggeado 
    AuthService.prototype.getAuth = function () {
        return this.afAuth.authState.map(function (auth) { return auth; });
    };
    AuthService.prototype.logout = function () {
        return this.afAuth.auth.signOut();
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [angularfire2_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./app/servicios/game.service.ts":
/*!***************************************!*\
  !*** ./app/servicios/game.service.ts ***!
  \***************************************/
/*! exports provided: GameService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameService", function() { return GameService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/database */ "../node_modules/angularfire2/database/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GameService = /** @class */ (function () {
    function GameService(firebase) {
        this.firebase = firebase;
    }
    GameService.prototype.getProducts = function () {
        return this.gameList = this.firebase.list('games');
    };
    GameService.prototype.insertGame = function (game) {
        console.log(game);
        console.log(this.gameList);
        this.gameList.push({
            player1: game.player1,
            player2: game.player2,
            pts1: game.pts1,
            pts2: game.pts2
        });
    };
    GameService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [angularfire2_database__WEBPACK_IMPORTED_MODULE_1__["AngularFireDatabase"]])
    ], GameService);
    return GameService;
}());



/***/ }),

/***/ "./app/servicios/global.ts":
/*!*********************************!*\
  !*** ./app/servicios/global.ts ***!
  \*********************************/
/*! exports provided: Global */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Global", function() { return Global; });
var Global = {
    url: "http://localhost:3000/"
};


/***/ }),

/***/ "./app/servicios/ingame.service.ts":
/*!*****************************************!*\
  !*** ./app/servicios/ingame.service.ts ***!
  \*****************************************/
/*! exports provided: IngameService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngameService", function() { return IngameService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./global */ "./app/servicios/global.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IngameService = /** @class */ (function () {
    function IngameService(_http) {
        this._http = _http;
        this.url = _global__WEBPACK_IMPORTED_MODULE_2__["Global"].url;
    }
    IngameService.prototype.tipoJuego = function (juego) {
        var params = JSON.stringify(juego);
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Content-Type', 'application/json');
        return this._http.post(this.url + "game-mode", params, { headers: headers });
    };
    IngameService.prototype.realizarJugada = function (x, y, jugador) {
        var params = JSON.stringify({ x: y, y: x, jugador: jugador });
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Content-Type', 'application/json');
        return this._http.post(this.url + "ingame", params, { headers: headers });
    };
    IngameService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], IngameService);
    return IngameService;
}());



/***/ }),

/***/ "./environments/environment.ts":
/*!*************************************!*\
  !*** ./environments/environment.ts ***!
  \*************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyB4Utw8Nf2bDS8iYeChxAGceRhLziRWvMw",
        authDomain: "proyectodiseno-212203.firebaseapp.com",
        databaseURL: "https://proyectodiseno-212203.firebaseio.com",
        projectId: "proyectodiseno-212203",
        storageBucket: "proyectodiseno-212203.appspot.com",
        messagingSenderId: "250102866434"
    }
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./main.ts":
/*!*****************!*\
  !*** ./main.ts ***!
  \*****************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***********************!*\
  !*** multi ./main.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\vinic\Documents\GitKraken\othello-diseno\login\src\main.ts */"./main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map