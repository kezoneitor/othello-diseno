export class Game{
    //El usuario puede ser o de tipo User o de tipo BOT
    constructor(
        tipo: number,
        tablero: number[][],
        jugador1: {ficha: number, usuario: Object, color: string},
        jugador2: {ficha: number, usuario: Object, color: string},
        jugadorActual: number,
        estado: number,
        ID: string,
        cantFichas1: number,
        cantFichas2: number
    ){ }
}