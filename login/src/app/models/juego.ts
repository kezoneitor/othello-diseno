export class Juego{
    constructor(
        public tipo: number,
        public jugador1: number,
        public nombreJ1: string,
        public jugador2: number,
        public nombreJ2: string,
        public dimension: number,
        public tablero: number[][],
        public jugador: number,
        public colorJ1: string,
        public colorJ2: string,
    ){ }
}