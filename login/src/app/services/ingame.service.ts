import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Juego } from '../models/juego';
import { Global } from './global';

@Injectable()
export class IngameService{
    public url: string;

    constructor(
        public _http: HttpClient
    ) {
        this.url = Global.url;
    }

    tipoJuego(juego: Juego): Observable<any>{
        let params = JSON.stringify(juego);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(this.url+"game-mode", params, {headers : headers});
    }

    realizarJugada(x, y, jugador): Observable<any>{
        let params = JSON.stringify({x: y, y: x, jugador: jugador});
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(this.url+"ingame", params, {headers : headers});
    }

    creaTablero(): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');        
        return this._http.get(this.url+"table", {headers: headers});
    }

    sesiones(key, jugador): Observable<any>{
        let params = JSON.stringify({"key":key, "jugador": jugador});
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(this.url+"sesion", params, {"headers" : headers});
    }
}