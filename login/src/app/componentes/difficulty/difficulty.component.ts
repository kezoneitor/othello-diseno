import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-difficulty',
  templateUrl: './difficulty.component.html',
  styleUrls: ['./difficulty.component.scss']
})
export class DifficultyComponent implements OnInit {

  constructor(public authService: AuthService,
    public router: Router) { }

  ngOnInit() {
  }

  easy(){
    this.authService.jug1="#0000FF";
    this.authService.jug2= "#FFFFFF";
    this.authService.size = 8;
    this.authService.tipo = 2;
    this.router.navigate(['/ingame']);
  }

  medium(){
    this.authService.jug1="#0000FF";
    this.authService.jug2= "#FFFFFF";
    this.authService.size = 8;
    this.authService.tipo = 3;
    this.router.navigate(['/ingame']);
  }

  hard(){
    this.authService.jug1="#0000FF";
    this.authService.jug2= "#FFFFFF";
    this.authService.size = 8;
    this.authService.tipo = 4;
    this.router.navigate(['/ingame']);
  }


}
