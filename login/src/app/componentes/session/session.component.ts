import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import { FormControl, Validators, FormBuilder, FormGroup, ReactiveFormsModule, NgForm } from '@angular/forms';
import { IngameService } from '../../services/ingame.service';

import { Juego } from '../../models/juego';


@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})

export class SessionComponent implements OnInit {
  public jug1: string;
  public jug2: string;
  public size: number;
  public tipo: number;
  public form: FormGroup;
  public tableData: Array<JSON>;
  public juego : Juego;

  searchText: string;
  /*tableData = [
    { id: '1', firstName: 'Mark', lastName: 'Otto'},
    { id: '2', firstName: 'Jacob', lastName: 'Thornton'},
    { id: '3', firstName: 'Larry', lastName: 'Last'},
    { id: '4', firstName: 'John', lastName: 'Doe'},
    { id: '5', firstName: 'Zigi', lastName: 'Kiwi'},
    { id: '6', firstName: 'Beatrice', lastName: 'Selphie' },
  ];*/


  constructor(public authService: AuthService,public router: Router,private _ingameService: IngameService) { 
  }
  
  ngOnInit() {
    this._ingameService.creaTablero().subscribe(
      result => {
        this.tableData = result.data;
        console.log(this.tableData);
      },
      error => {
        console.log(<any>error);
      }
    )
  }

  goToGame(key){
    var banderita=false;
    this.tableData.forEach(function(elem){
      if(elem["key"]==key){
        banderita = true;
        console.log(elem["key"]);
      }
    });

    if(banderita){
      this.authService.key = key;
      this.authService.tipo = 1;
      
      this.router.navigate(['/ingame']);
    }
  }

  filterIt(arr, searchKey) {
    return arr.filter((obj) => {
      return Object.keys(obj).some((key) => {
        return obj[key].includes(searchKey);
      });
    });
  }

  search(){
    if (!this.searchText) {
      return this.tableData;
    }
    if (this.searchText) {
      return this.filterIt(this.tableData, this.searchText);
    }
  }

  sessionData(){
    this.juego = new Juego(1,1,this.authService.nombreJ1,2,null,this.size,[], 1,this.jug1,this.jug2);
    this._ingameService.tipoJuego(this.juego).subscribe(
      result => {
        alert(result.message);
      },
      error => {
        console.log(<any>error);
      }
    )
  }


}
