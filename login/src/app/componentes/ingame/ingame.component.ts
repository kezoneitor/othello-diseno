import { Component, OnInit } from '@angular/core';
import { Juego } from '../../models/juego';
import { Game } from '../../models/game';
import { IngameService } from '../../services/ingame.service';
import {AuthService} from '../../services/auth.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'ingame',
  templateUrl: './ingame.component.html',
  styleUrls: ['./ingame.component.scss'],
  providers: [IngameService]
})

export class IngameComponent implements OnInit {
  public juego : Juego;
  public game: Game;
  public j1_color: string;
  public j2_color: string;

  constructor(
    public authService: AuthService,
    private _ingameService: IngameService,
  ) {
   }

  ngOnInit() {
    if(this.authService.tipo!=1){
      this.juego = new Juego(this.authService.tipo,1,this.authService.nombreJ1,2,"Bot",8,[], 1,this.authService.jug1,this.authService.jug2);
      this._ingameService.tipoJuego(this.juego).subscribe(
        result => {
          this.juego = result.tablero;
          this.j1_color = "black";
          this.j2_color = "white";
          //console.log("RESULTADO "+ this.juego.tablero);
        },
        error => {
          console.log(<any>error);
        }
      )
    }
    else{
      this.juego = new Juego(this.authService.tipo,1,"",2,this.authService.nombreJ1,0,[], 1,this.authService.jug1,this.authService.jug2);
      console.log("Nombre del jugador 1 ", this.authService.nombreJ1);
      this._ingameService.sesiones(this.authService.key, this.authService.nombreJ1).subscribe(
      result => {
        this.juego = result.object;
        /*this.juego.tablero = result.object.tablero;*/
        this.j1_color = result.object.colorJ1;
        this.j2_color = result.object.colorJ2;
      },
      error => {
        console.log(<any>error);
      });
    }
  }

  realizarJugada(x, y){
    this._ingameService.realizarJugada(x, y, this.juego.jugador).subscribe(
      result => {
        this.juego.tablero = result.tablero;
      },
      error => {
        console.log(<any>error);
      }
    )
  }

}
