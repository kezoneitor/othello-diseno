import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import { IngameService } from '../../services/ingame.service';


@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {
  searchText: string;
  tableData = [
    { id: '1', firstName: 'Mark', lastName: 'Otto'},
    { id: '2', firstName: 'Jacob', lastName: 'Thornton'},
    { id: '3', firstName: 'Larry', lastName: 'Last'},
    { id: '4', firstName: 'John', lastName: 'Doe'},
    { id: '5', firstName: 'Zigi', lastName: 'Kiwi'},
    { id: '6', firstName: 'Beatrice', lastName: 'Selphie' },
  ];
  constructor(public authService: AuthService,public router: Router,private _ingameService: IngameService) { }

  ngOnInit() {
  }

  filterIt(arr, searchKey) {
    return arr.filter((obj) => {
      return Object.keys(obj).some((key) => {
        return obj[key].includes(searchKey);
      });
    });
  }

  search(){
    if (!this.searchText) {
      return this.tableData;
    }
    if (this.searchText) {
      //console.log(this.searchText);
      return this.filterIt(this.tableData, this.searchText);
    }
  }

}
