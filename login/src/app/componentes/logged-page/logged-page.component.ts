import { Component, OnInit } from '@angular/core';
import {AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';


@Component({
  selector: 'app-logged-page',
  templateUrl: './logged-page.component.html',
  styleUrls: ['./logged-page.component.scss']
})
export class LoggedPageComponent implements OnInit {
  public nombreUsuario: string;
  public email: string;
  public user: User;
  constructor(
    public authService: AuthService,
    public userService: UserService
    ) {  }

  ngOnInit() {
    this.authService.getAuth().subscribe( auth => { 
      this.user = new User(auth.displayName, auth.email,[]);
      this.authService.nombreJ1 = auth.displayName;       
      this.userService.login(this.user).subscribe(
          result => {
            this.nombreUsuario = result.nombreUsuario;
            this.email = result.correo;
          },
          error => {
            console.log(error);
          }
        )
    });
  }

}
