import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { LoginPageComponent } from './componentes/login-page/login-page.component';
import { LoggedPageComponent } from './componentes/logged-page/logged-page.component';
import { RegisterPageComponent } from './componentes/register-page/register-page.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import {HomePageComponent} from './componentes/home-page/home-page.component';
import { GameModeComponent } from './componentes/game-mode/game-mode.component';
import { DifficultyComponent } from './componentes/difficulty/difficulty.component';
import { IngameComponent } from './componentes/ingame/ingame.component';

import {FlashMessagesModule} from 'angular2-flash-messages';
import {FlashMessagesService} from 'angular2-flash-messages';


import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';

import {environment} from '../environments/environment';

import { AuthService } from './services/auth.service';
import { IngameService } from './services/ingame.service';

import {AuthGuard} from './guards/auth.guard';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { HttpClientModule } from '@angular/common/http';
import { SessionComponent } from './componentes/session/session.component';
import { UserService } from './services/user.service';
import { ResumeComponent } from './componentes/resume/resume.component';


@NgModule({
  declarations: [
    AppComponent,
    LoggedPageComponent,
    NavbarComponent,
    RegisterPageComponent,
    LoginPageComponent,
    HomePageComponent,
    GameModeComponent,
    DifficultyComponent,
    IngameComponent,
    SessionComponent,
    ResumeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    FlashMessagesModule,
    AngularFireDatabaseModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [AuthService,FlashMessagesService,AuthGuard, IngameService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
