import * as functions from 'firebase-functions';
import { user } from '../node_modules/firebase-functions/lib/providers/auth';


var admin = require('firebase-admin');

var serviceAccount = require('../serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://proyectodiseno-212203.firebaseio.com'
});

var db = admin.firestore();

var usuarios = db.collection("Usuarios");



 export const getUser = functions.https.onRequest((request, response) => {
    if(request.method=="GET"){
        usuarios.get()
        .then((userList)=>{
            var res = [];
            userList.forEach((users)=>{
              res.push({name:users.name,data:users.data()});  
            })
            response.send({status:true,data:res});
        })
        .catch(err =>response.send({status:false,data:"Error"}))
    }
    else if(request.method=="POST"){

        usuarios.doc(request.body.id).set({nombre:request.body.nombre, cedula:request.body.cedula});
    }

    else{
        response.send({status:true,data:"Solo admite GET"})
    }
 });




 /*export const postUser = functions.https.onRequest((request, response) => {
    if(request.method=="POST"){
        console.log("Hola");
        usuarios.doc(request.body.id).set({nombre:request.body.nombre});
    }

    else{
        response.send({status:true,data:"Solo admite GET"})
    }
 });
*/