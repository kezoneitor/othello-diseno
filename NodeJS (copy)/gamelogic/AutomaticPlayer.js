const JugadasPosibles = require("./JugadasPosibles.js");

module.exports = class AutomaticPlayer{
	constructor(nivel, dimension){
		this._nivel = nivel;
		this._dimension = dimension;
	}

	solicitarJugada(matriz, jugador){
		if(this._nivel == 2){
			return this.jugadaFacil(matriz, jugador, this._dimension);
		}else if(this._nivel == 3){
			return this.jugadaMedia(matriz, jugador, this._dimension);
		}else{
			return this.jugadaDificil(matriz, jugador, this._dimension);
		}
	}

	jugadaFacil(matriz, jugador, dimension){
		let jugadasPosibles = new JugadasPosibles();
		return jugadasPosibles.obtenerJugadaFacil(matriz, jugador, dimension);
	}

	jugadaMedia(matriz, jugador, dimension){
		let jugadasPosibles = new JugadasPosibles();
		return jugadasPosibles.obtenerJugadaMedia(matriz, jugador, dimension);
	}

	jugadaDificil(matriz, jugador, dimension){
		let jugadasPosibles = new JugadasPosibles();
		return jugadasPosibles.obtenerJugadaDificil(matriz, jugador, dimension);
	}
};