const LinkedList = require("../datastructures/LinkedList.js");
//probablemente aquí hay que hacer un let listaJugadas;


module.exports = class JugadasPosibles{
	constructor(){

	}

	obtenerJugadas(matriz, jugadorContrincario, dimension){
		let listaJugadas = new LinkedList();
		let posicionesVaciasTablero = this.posicionesVaciasTablero(matriz, dimension);
		let posicionesVaciasConAdyacentes = new LinkedList();
		for(var i = 0; i < posicionesVaciasTablero.length; i++){
			if(this.tieneAdyacentes(matriz, posicionesVaciasTablero.get(i).data, dimension, jugadorContrincario) == true){
				posicionesVaciasConAdyacentes.add(posicionesVaciasTablero.get(i).data);
			}
		}
		return this.evaluarPosicionesVaciasConAdyacentesContrincario(matriz, jugadorContrincario, dimension, posicionesVaciasConAdyacentes);
	}

	evaluarPosicionesVaciasConAdyacentesContrincario(matriz, jugadorContrincario, dimension, posicionesVaciasConAdyacentes){
		let fichasQueCompletanUnaJugada = new LinkedList();
		var jugador = 2;
		if(jugadorContrincario == 2)
			jugador = 1;

		for(var i = 0; i < posicionesVaciasConAdyacentes.length; i++){
			let listaAdyacentes = new LinkedList();
			listaAdyacentes = this.obtenerPosicionesAdyacentes(listaAdyacentes, posicionesVaciasConAdyacentes.get(i).data[0], posicionesVaciasConAdyacentes.get(i).data[1], dimension - 1);

			for(var j = 0; j < listaAdyacentes.length; j++){
				var coordIni = listaAdyacentes.get(j).data;
				let aux = new LinkedList();
				var cantFichas = this.evaluarCaminoDeFichasOponentes(matriz, coordIni[0], coordIni[1], coordIni[0], coordIni[1], posicionesVaciasConAdyacentes.get(i).data[0], posicionesVaciasConAdyacentes.get(i).data[1], jugadorContrincario, jugador, 0);
				if(cantFichas > 0){
					aux.add(posicionesVaciasConAdyacentes.get(i).data);
					aux.add(cantFichas);
					fichasQueCompletanUnaJugada.add(aux);
				}
			}
			
		} 
		return fichasQueCompletanUnaJugada;
	}

	evaluarCaminoDeFichasOponentes(matriz, posx, posy, iniX, iniY, x, y, jugadorContrario, jugador, cantFichas){

		try{
			if(posx < 0 || posy < 0){
				return -1;
			}
			else{
				if(matriz[posx][posy] == jugadorContrario){
					cantFichas = cantFichas + 1;
					return this.evaluarCaminoDeFichasOponentes(matriz, (iniX + (posx - x)), (iniY + (posy - y)), iniX, iniY, x, y, jugadorContrario, jugador, cantFichas);
				}else if(matriz[posx][posy] == jugador){

					return cantFichas;
				}else{
					return -1;
				}
			}
		}catch(error){
			console.log("Errors: " + error);
			return -1;
		}
	}

	posicionesVaciasTablero(matriz, dimension){
		let posicionesVaciasTableroJuego = new LinkedList();
		for(var i = 0; i < dimension; i++){
			for(var j = 0; j < dimension; j++){
				if(matriz[i][j] == 0)
					posicionesVaciasTableroJuego.add([i, j]);
			}
		}	
		return posicionesVaciasTableroJuego;
	}

	obtenerJugadaFacil(matriz, jugador, dimension){
		console.log("fácil");
		let listaJugadas = this.obtenerJugadas(matriz, jugador, dimension);
		if(listaJugadas.length > 0){
			return this.obtenerJugadaConMenosFichas(listaJugadas);
		}else{
			return -1; //No se encontró ninguna jugada válida, por lo que pasa el turno al jugador contrario
		}
	}

	obtenerJugadaConMenosFichas(listaJugadas){
		var temp;
		var count = 100; //valor alto, de modo que se comience a buscar el menor
		for(var i = 0; i < listaJugadas.length; i++){
			if(listaJugadas.get(i).data.get(1).data < count){
				temp = listaJugadas.get(i).data.get(0).data;
				count = listaJugadas.get(i).data.get(1).data;
			}
		}
		return temp;
	}

	obtenerJugadaMedia(matriz, jugador, dimension){
		console.log("medio");
		let listaJugadas = this.obtenerJugadas(matriz, jugador, dimension);
		if(listaJugadas.length > 0){
			return this.obtenerValorAleatorio(listaJugadas);
		}else{
			return -1; //No se encontró ninguna jugada válida, por lo que pasa el turno al jugador contrario
		}
	}

	obtenerValorAleatorio(listaJugadas){
		var temp;
		var index = Math.random(listaJugadas.length);
		temp = listaJugadas.get(index).data.get(0).data;
		return temp;
	}

	obtenerJugadaDificil(matriz, jugador, dimension){
		console.log("difícil");
		let listaJugadas = this.obtenerJugadas(matriz, jugador, dimension);
		if(listaJugadas.length > 0){
			return this.obtenerJugadaConMasFichas(listaJugadas);
		}else{
			return -1; //No se encontró ninguna jugada válida, por lo que pasa el turno al jugador contrario
		}
	}

	obtenerJugadaConMasFichas(listaJugadas){
		var temp;
		var count = 0;
		for(var i = 0; i < listaJugadas.length; i++){
			if(listaJugadas.get(i).data.get(1).data > count){
				temp = listaJugadas.get(i).data.get(0).data;
				count = listaJugadas.get(i).data.get(1).data;
			}
		}		
		return temp;
	}

	tieneAdyacentes(matriz, posicion, dimension, jugadorContrincario){
		var tieneAdyacentesContrincario = false;
		let listaAdyacentes = new LinkedList();
		var borde = dimension - 1;
		listaAdyacentes = this.obtenerPosicionesAdyacentes(listaAdyacentes, posicion[0], posicion[1], borde);
		for(var i = 0; i < listaAdyacentes.length; i++){
			if(matriz[listaAdyacentes.get(i).data[0]][listaAdyacentes.get(i).data[1]] == jugadorContrincario)
				tieneAdyacentesContrincario = true;
		}
		
		return tieneAdyacentesContrincario;
	}

	esquinaSuperiorIzquierda(listaAdyacentes){
		listaAdyacentes.add([1, 0]);
		listaAdyacentes.add([1, 1]);
		listaAdyacentes.add([0, 1]);
		return listaAdyacentes;
	}

	esquinaInferiorIzquierda(listaAdyacentes, borde){
		listaAdyacentes.add([1, borde]);
		listaAdyacentes.add([1, borde - 1]);
		listaAdyacentes.add([0, borde - 1]);
		return listaAdyacentes;
	}

	esquinaSuperiorDerecha(listaAdyacentes, borde){
		listaAdyacentes.add([borde - 1, 0]);
		listaAdyacentes.add([borde - 1, 1]);
		listaAdyacentes.add([borde, 1]);
		return listaAdyacentes;
	}

	esquinaInferiorDerecha(listaAdyacentes, borde){
		listaAdyacentes.add([borde - 1, borde]);
		listaAdyacentes.add([borde - 1, borde - 1]);
		listaAdyacentes.add([borde, borde - 1]);
		return listaAdyacentes;
	}

	bordeIzquierdo(listaAdyacentes, y){
		listaAdyacentes.add([0, y - 1]);
		listaAdyacentes.add([1, y - 1]);
		listaAdyacentes.add([1, y]);
		listaAdyacentes.add([1, y + 1]);
		listaAdyacentes.add([0, y + 1]);
		return listaAdyacentes;
	}

	bordeDerecho(listaAdyacentes, y, borde){
		listaAdyacentes.add([borde, y - 1]);
		listaAdyacentes.add([borde - 1, y - 1]);
		listaAdyacentes.add([borde - 1, y]);
		listaAdyacentes.add([borde - 1, y + 1]);
		listaAdyacentes.add([borde, y + 1]);
		return listaAdyacentes;
	}

	bordeSuperior(listaAdyacentes, x){
		listaAdyacentes.add([x - 1, 0]);
		listaAdyacentes.add([x - 1, 1]);
		listaAdyacentes.add([x, 1]);
		listaAdyacentes.add([x + 1, 1]);
		listaAdyacentes.add([x + 1, 0]);
		return listaAdyacentes;
	}

	bordeInferior(listaAdyacentes, x, borde){
		listaAdyacentes.add([x - 1, borde]);
		listaAdyacentes.add([x - 1, borde - 1]);
		listaAdyacentes.add([x, borde - 1]);
		listaAdyacentes.add([x + 1, borde - 1]);
		listaAdyacentes.add([x + 1, borde]);
		return listaAdyacentes;
	}

	posicionesCentrales(listaAdyacentes, x, y){
        listaAdyacentes.add([x - 1, y - 1]);
		listaAdyacentes.add([x, y - 1]);
		listaAdyacentes.add([x + 1, y - 1]);
		listaAdyacentes.add([x + 1, y]);
		listaAdyacentes.add([x + 1, y + 1]);
		listaAdyacentes.add([x, y + 1]);
		listaAdyacentes.add([x - 1, y + 1]);
		listaAdyacentes.add([x - 1, y]);
		return listaAdyacentes;
	}

	obtenerPosicionesAdyacentes(listaAdyacentes, x, y, borde){
		if(x == 0 && y == 0)
			listaAdyacentes = this.esquinaSuperiorIzquierda(listaAdyacentes);
		else if(x == 0 && y == borde)
			listaAdyacentes = this.esquinaInferiorIzquierda(listaAdyacentes, borde);
		else if(x == borde && y == 0)
			listaAdyacentes = this.esquinaSuperiorDerecha(listaAdyacentes, borde);
		else if(x == borde && y == borde)
			listaAdyacentes = this.esquinaInferiorDerecha(listaAdyacentes, borde);
		else if(x == 0)
			listaAdyacentes = this.bordeIzquierdo(listaAdyacentes, y);
		else if (x == borde)
			listaAdyacentes = this.bordeDerecho(listaAdyacentes, y, borde);
		else if(y == 0)
			listaAdyacentes = this.bordeSuperior(listaAdyacentes, x);
		else if(y == borde)
			listaAdyacentes = this.bordeInferior(listaAdyacentes, x, borde);
		else
			listaAdyacentes = this.posicionesCentrales(listaAdyacentes, x, y);
		return listaAdyacentes;
	}
};