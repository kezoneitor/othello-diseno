/*var express = require('express');
var app = express();
const Juego = require("./gamelogic/Juego.js");
const AutomaticPlayer = require("./gamelogic/AutomaticPlayer.js");
var cors = require("cors");
var bodyParser = require("body-parser");
var sleep = require("thread-sleep");
let instanciaJuego;
let automaticPlayer;

app.use(bodyParser.urlencoded({ extended: true}));
app.use(cors());
app.use(bodyParser.json());*/

var express = require('express');
var app = express();
const Juego = require("./gamelogic/Juego.js");
const AutomaticPlayer = require("./gamelogic/AutomaticPlayer.js");
var cors = require("cors");
var bodyParser = require("body-parser");
let instanciaJuego;
var admin = require("firebase-admin");
let automaticPlayer;
let isBot;

app.use(bodyParser.urlencoded({ extended: true}));
app.use(cors());
app.use(bodyParser.json());

//init firebase account 
var serviceAccount = require('./serviceAccountKey.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://proyectodiseno-212203.firebaseio.com'
});

var db = admin.database();

app.post('/ingame', function(req, res){
	console.log(req.body);
	var coordX = req.body.x;
	var coordY = req.body.y;
	var jugador = req.body.jugador;
	console.log("Jugadorcito jiji: " + jugador);
	instanciaJuego.realizarJugada(coordX, coordY, jugador);
	var jugSig = 2;
	if(jugador == 2)
		jugSig = 1;
	if(isBot){
		var jugadaAuto = automaticPlayer.solicitarJugada(instanciaJuego._tablero, jugSig);
		instanciaJuego.realizarJugada(jugadaAuto[1], jugadaAuto[0], jugSig);
	}
	if(instanciaJuego._jugadaValida == true){
		instanciaJuego._jugadaValida = false;
		let gameKey;
		db.ref("Partidas").once("value", snapshot => {
			snapshot.forEach(element => {
				if(element.val().nombreJ1 == req.body.nombreJ1
				&& element.val().nombreJ2 == req.body.nombreJ2){
					gameKey = element.key;
					console.log(gameKey);
				}
			});
		})
		.then(() => {
			req.body.tablero = instanciaJuego._tablero;
			db.ref("Partidas/"+ gameKey).set(req.body);
		}).then(() => {
			res.json({jugadaValida:true, mensaje:"Jugada exitosa", tablero_lleno:instanciaJuego.tablaLlena(), tablero:instanciaJuego._tablero, jugador:jugSig});
		})
		.catch(error => {
			console.log("ERROR Actualizar Jugada Realizada//",error);
		});
	}else{
		res.json({jugadaValida:false, mensaje:"Jugada no válida", tablero_lleno:false, tablero:instanciaJuego._tablero, jugador:jugSig});
	}

	//Aún falta definir como va a ser lo del ganador
	
});

app.post('/login', function(req, res){
	let exist = false; 
	let user;
	db.ref("Usuarios").once("value", 
		function(snapshot) {
			snapshot.forEach(element => {
				if(req.body.correo == element.val().correo){
					user = {"nombreUsuario": req.body.nombreUsuario, "correo": req.body.correo};
					exist = true;
				}
			});
		},
		error => {
			console.log(error);
		}).then(() => {
			if(exist == false){
				user = {"nombreUsuario": req.body.nombreUsuario, "correo": req.body.correo};
				db.ref("Usuarios").push(user);
			}
			res.json(user);	
		});
});

app.get('/table', function(req,res){
	arr = []
	var partidas = db.ref("Partidas");
	partidas.orderByValue().on("value", function(snapshot) {
	let count = 1;
	snapshot.forEach(function(data) {  
		var item = data.val();
		item.key = data.key;
		item.index = count
		arr.push(item);
		count++;  	
  	});
	res.json({data: arr})
	
	});
});


function updateUser(name, gameKey){
	let user;
	let key;
	db.ref("Usuarios").once("value", function(snapshot){
		snapshot.forEach(element => {
			if(element.val().nombreUsuario == name){
				user = element.val();
				key = element.key;
			}
		});
	})
	.then(function(){
		console.log(name, user);
		arr = [];
		if(user.resumes){
			arr = user.resumes; 
		}
		arr.push(gameKey);
		db.ref("Usuarios/"+ key).set({
			"nombreUsuario" : user.nombreUsuario,
			"correo" : user.correo,
			"resumes" : arr
		});
	})
	.catch(function(error){
		console.log("Aahhh, perro ", error);
	});
}
	

app.post('/sesion', function(req, res){
	let key;
	let partida;
	db.ref("Partidas").once("value", 
		function(snapshot) {
			snapshot.forEach(element => {
				//console.log(element.key);
				if(req.body.key == element.key){
					key = element.key;
					console.log(`Juego que se esta buscando `, element.val());
					res.json({object: element.val()});
				}
			});
		},
		error => {
			console.log(error);
		});
});




app.post('/game-mode', function(req, res){
	console.log(req.body);
	var tipo = req.body.tipo;
	var jugador1 = req.body.jugador1;
	var jugador2 = req.body.jugador2;
	var dimension = req.body.dimension;
	var nombreJ1 = req.body.nombreJ1;
	if(tipo == 1){
		console.log("Psss, here")
		isBot = false;
		instanciaJuego = new Juego(dimension);
		//2: fácil, 3: medio, 4: difícil
		//automaticPlayer = new AutomaticPlayer(2, dimension);
		console.log("Jugador 1: " + jugador1);
		console.log("Jugador 2: " + jugador2);
		instanciaJuego.inicializarTablero();
		req.body.tablero = instanciaJuego._tablero;
		db.ref("Partidas").push(req.body)
		.then(() => {
			res.json({message: "Partida creada existosamente"});
		});
	}else if(tipo == 2){
		console.log("Juego 1vsCPU fácil");
		//res.send("Juego 1vsCPU fácil");
		isBot = true;
		instanciaJuego = new Juego(dimension);
		//2: fácil, 3: medio, 4: difícil
		automaticPlayer = new AutomaticPlayer(2, dimension);
		instanciaJuego.inicializarTablero();
		var response = {indicador:true, mensaje:"Juego aceptado", tablero:instanciaJuego._tablero};
		res.json(response);
	}else if(tipo == 3){
		isBot = true;
		instanciaJuego = new Juego(dimension);
		//2: fácil, 3: medio, 4: difícil
		automaticPlayer = new AutomaticPlayer(3, dimension);
		instanciaJuego.inicializarTablero();
		var response = {indicador:true, mensaje:"Juego aceptado", tablero:instanciaJuego._tablero};
		res.json(response);
		console.log("Juego 1vsCPU medio");
		//res.send("Juego 1vsCPU medio");
	}else{
		console.log("Juego 1vsCPU difícil");
		//res.json({mensaje:"Juego 1vsCPU dificil"});
		isBot = true;
		instanciaJuego = new Juego(dimension);
		//2: fácil, 3: medio, 4: difícil
		automaticPlayer = new AutomaticPlayer(4, dimension);
		instanciaJuego.inicializarTablero();
		var response = {indicador:true, mensaje:"Juego aceptado", tablero:instanciaJuego._tablero};
		res.json(response);
	}
});

app.listen(3000, function () {
    console.log('Servidor de Othello corriendo en puerto 3000!');
});
